#ColourAnalog

Simple Pebble Watchface

Aiming to allow lots of configuration options.

![](screenshots/basalt.gif?raw=true)
![](screenshots/aplite.gif?raw=true)
![](screenshots/chalk.gif?raw=true)

![](screenshots/settings.png?raw=true)

Donations | Bitcoin 3Dr7HHJEd5sNyht9J8gi5dN6QoUnxRdjZW | Ethereum Classic 0x3F45Ba445B5738E709A2A28c3A199B800eEb96Fc

