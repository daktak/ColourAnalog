/*
 * date.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "point.h"
static int width = 14*6;
static int height = 36;
TextLayer *s_date_text;

void set_date_text() {
  ClaySettings settings = get_settings();
  time_t now = time(NULL);
  struct tm *t = localtime(&now);
  static char date_text[sizeof("Mon Jan 31")];
  char *format = "";
  switch (settings.date_format) {
        case 0: // Mon // Oct 22 (date)
            format = "%a %b %d";
            break;
        case 1: // Oct 22 (date)
            format = "%b %d";
            break;
        case 2: // 10/22 (date)
            format = "%m/%d";
            break;
        case 3: // 22.10. (date)
            format = "%d.%m.";
            break;
        case 4: // 22 (date)
            format = "%d";
            break;
        case 5: // Mon 22 (date)
            format = "%a %d";
            break;
        case 6: // Mon (day)
            format = "%a";
            break;
  }
  strftime(date_text, sizeof(date_text), format, t);
  if (s_date_text != NULL) {
    text_layer_set_text(s_date_text, date_text);
  }
}

Layer* create_date_layer(ClaySettings settings, GPoint points[]) {
  int pos = get_position(DATE);
  GPoint start = center_point(points, width, height, pos);
  if (s_date_text == NULL) {
    s_date_text = text_layer_create (GRect(start.x, start.y, width, height));
  }
  text_layer_set_background_color(s_date_text, GColorClear);
  text_layer_set_text_color(s_date_text, GColorFromHEX(settings.date_color));
  text_layer_set_font(s_date_text, fonts_get_system_font(FONT_KEY_GOTHIC_14));
  text_layer_set_overflow_mode(s_date_text, GTextOverflowModeWordWrap);
  text_layer_set_text_alignment(s_date_text, GTextAlignmentCenter);
  //set_date_text();
  return text_layer_get_layer(s_date_text);
}
