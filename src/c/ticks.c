/*
 * ticks.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include <pebble.h>
#include "main.h"

/**
 * Returns a point on the line from the center away at a given angel, at an offset from the border.
 */
GPoint get_radial_border_point(GPoint s_center, const int16_t distance_from_border, const int32_t angle, int16_t width, int16_t height) {

  const int32_t topright_angle = atan2_lookup(width, height);
  bool top = angle > (TRIG_MAX_ANGLE - topright_angle) || angle <= topright_angle;
  bool bottom = angle > TRIG_MAX_ANGLE/2 - topright_angle && angle <= TRIG_MAX_ANGLE/2 + topright_angle;
  bool left = angle > TRIG_MAX_ANGLE/2 + topright_angle && angle <= TRIG_MAX_ANGLE - topright_angle;
  int32_t sine = sin_lookup(angle);
  int32_t cosine = cos_lookup(angle);
  if (top || bottom) {
      GPoint result = {
        .x = (int16_t) (top ? 1 : -1) * (sine * (int32_t) (height / 2 - distance_from_border) / cosine) + s_center.x,
        .y = top ? distance_from_border : height - distance_from_border,
      };
      return result;
  }
  // assert(left || right);
  GPoint result = {
    .x = left ? distance_from_border : width - distance_from_border,
    .y = (int16_t) (left ? 1 : -1) * (cosine * (int32_t) (width / 2 - distance_from_border) / sine) + s_center.y,
  };
  return result;
}

/**
 *  * Returns a point on the line from the center away at an angle specified by tick/maxtick, at a specified distance
 *   */
GPoint get_radial_point(GPoint s_center, const int16_t distance_from_center, const int32_t angle) {
  GPoint result = {
    .x = (int16_t) (sin_lookup(angle) * (int32_t) distance_from_center / TRIG_MAX_RATIO) + s_center.x,
    .y = (int16_t) (-cos_lookup(angle) * (int32_t) distance_from_center / TRIG_MAX_RATIO) + s_center.y,
  };
  return result;
}

void draw_hour_round_ticks(GPoint center, GColor color, GContext *ctx, int16_t width) {
  graphics_context_set_stroke_color(ctx, color);
  uint16_t radius = width / 2;
  for (int i = 0; i < 12; ++i) {
    int32_t angle = i * TRIG_MAX_ANGLE / 12;
    int tick_length = 12;
    graphics_context_set_stroke_width(ctx, 2);
    graphics_draw_line(ctx, get_radial_point(center, radius + 3, angle),
                                                      get_radial_point(center, radius - tick_length, angle));
  }
}

void draw_hour_square_ticks(GPoint center, GColor color, GContext *ctx, int16_t width, int16_t height) {
  graphics_context_set_stroke_color(ctx, color);
  for (int i = 0; i < 12; ++i) {
    int32_t angle = i * TRIG_MAX_ANGLE / 12;
    int tick_length = (i % 3 == 0) ? 12 : 8;
    graphics_context_set_stroke_width(ctx, 4);
    graphics_draw_line(ctx, get_radial_border_point(center, 0, angle, width, height), get_radial_border_point(center, tick_length, angle, width, height));
  }
}

void draw_minute_round_ticks(GPoint center, GColor color, GContext *ctx, int16_t width) {
  graphics_context_set_stroke_color(ctx, color);
  uint16_t radius = width / 2;
  for (int i = 0; i < 60; ++i) {
    if (i % 5 != 0) {
      int32_t angle = i * TRIG_MAX_ANGLE / 60;
      graphics_context_set_stroke_width(ctx,1);
      graphics_draw_line(ctx, get_radial_point(center, radius + 3, angle), get_radial_point(center, radius - 5, angle));
    }
  };
}

void draw_minute_square_ticks(GPoint center, GColor color, GContext *ctx, int16_t width, int16_t height) {
  graphics_context_set_stroke_color(ctx, color);
  for (int i = 0; i < 60; ++i) {
    if (i % 5 != 0) {
      int32_t angle = i * TRIG_MAX_ANGLE / 60;
      graphics_context_set_stroke_width(ctx,1);
      graphics_draw_line(ctx, get_radial_border_point(center, 0, angle, width, height), get_radial_border_point(center, 4, angle, width, height));
    }
  }
}

void draw_ticks(GPoint s_center, ClaySettings settings, GContext *ctx, int16_t width, int16_t height) {
  GColor hour_color = GColorFromHEX(settings.ht_color);
  GColor minute_color = GColorFromHEX(settings.mt_color);
  if (ROUND) {
    draw_hour_round_ticks(s_center, hour_color, ctx, width);
    draw_minute_round_ticks(s_center, minute_color, ctx, width);
  } else {
    draw_hour_square_ticks(s_center, hour_color, ctx, width, height);
    draw_minute_square_ticks(s_center, minute_color, ctx, width, height);
  }
}
