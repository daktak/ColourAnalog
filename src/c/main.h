/*
 * main.h
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */
#pragma once

#ifndef MAIN_H
#define MAIN_H

#include <pebble.h>
#include <pebble-events/pebble-events.h>
#include "settings.h"
#include <pebble-bluetooth-icon/pebble-bluetooth-icon.h>
#include <pebble-battery-bar/pebble-battery-bar.h>
#include <pebble-simple-health/pebble-simple-health.h>
#include <pebble-generic-weather/pebble-generic-weather.h>
#include <pebble-phone-batt-bar/pebble-phone-batt-bar.h>

#define COLORS PBL_IF_COLOR_ELSE(true, false)
#define ROUND PBL_IF_ROUND_ELSE(true, false)
#define ANTIALIASING true
#define HAND_MARGIN 20
#define PBB_KEY 70

#define BATTERY 1
#define BLUETOOTH 2
#define DATE 3
#define TIME 4
#define HEALTH 5
#define WEATHER 6
#define PHONE 7
//#define DEBUG

typedef struct {
  uint8_t hours;
  uint8_t minutes;
  uint8_t seconds;
} Time;

void redraw();
void set_ticker();
void reload_bt();
void reload_batt();
void reload_pbb();
void reload_health();
void reload_weather();
void reload_date();
void reload_time();
void layer_safe_destroy(Layer *layer);

extern void set_time_text();
extern Layer* create_time_layer(ClaySettings settings, GPoint points[]);

extern void set_date_text();
extern Layer* create_date_layer(ClaySettings settings, GPoint points[]);

extern PhoneBattBarLayer* create_phone_batt_bar(ClaySettings settings, GPoint points[]);
extern BatteryBarLayer* create_batt_bar(ClaySettings settings, GPoint points[]);
extern BluetoothLayer* create_bt_layer(ClaySettings settings, GPoint points[]);

extern void draw_hands(Time mode_time, ClaySettings settings, GContext *ctx, uint8_t radius, GPoint center, bool temporary_second);
extern void draw_ticks(GPoint s_center, ClaySettings settings, GContext *ctx, int16_t width, int16_t height);
#endif /* !MAIN_H */
