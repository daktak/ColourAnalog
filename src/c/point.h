/*
 * point.h
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */
#pragma once
#ifndef POINT_H
#define POINT_H

#include "main.h"
GPoint center_point(GPoint points[], int width, int height, int pos);

#endif /* !POINT_H */
