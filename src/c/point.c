/*
 * point.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "main.h"
#include "point.h"

GPoint center_point(GPoint points[], int width, int height, int pos) {
  GPoint centered = points[pos];
  centered.y = centered.y - (height/2);
  centered.x = centered.x - (width/2);
  return centered;
}

