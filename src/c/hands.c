/*
 * hands.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */
#include <pebble.h>
#include "main.h"

void draw_hands(Time mode_time, ClaySettings settings, GContext *ctx, uint8_t radius, GPoint center, bool temporary_second) {
  // Adjust for minutes through the hour
  float minute_angle = TRIG_MAX_ANGLE * mode_time.minutes / 60;
  float hour_angle;
  hour_angle = TRIG_MAX_ANGLE * mode_time.hours / 12;
  hour_angle += (minute_angle / TRIG_MAX_ANGLE) * (TRIG_MAX_ANGLE / 12);

  GColor hour_color = GColorFromHEX(settings.hh_color);
  GColor minute_color = GColorFromHEX(settings.mh_color);
  GColor seconds_color = GColorFromHEX(settings.sh_color);

  // Plot hands
  GPoint minute_hand = (GPoint) {
    .x = (int16_t)(sin_lookup(minute_angle) * (int32_t)(radius - HAND_MARGIN) / TRIG_MAX_RATIO) + center.x,
    .y = (int16_t)(-cos_lookup(minute_angle) * (int32_t)(radius - HAND_MARGIN) / TRIG_MAX_RATIO) + center.y,
  };
  GPoint hour_hand = (GPoint) {
    .x = (int16_t)(sin_lookup(hour_angle) * (int32_t)(radius - (2 * HAND_MARGIN)) / TRIG_MAX_RATIO) + center.x,
    .y = (int16_t)(-cos_lookup(hour_angle) * (int32_t)(radius - (2 * HAND_MARGIN)) / TRIG_MAX_RATIO) + center.y,
  };

  // Draw hands with positive length only
  if (radius > 2 * HAND_MARGIN) {
    graphics_context_set_stroke_color(ctx, hour_color);
    graphics_context_set_stroke_width(ctx, 2);
    graphics_draw_line(ctx, center, hour_hand);
  }
  if (radius > HAND_MARGIN) {
    graphics_context_set_stroke_color(ctx, minute_color);
    graphics_context_set_stroke_width(ctx, 1);
    graphics_draw_line(ctx, center, minute_hand);
    if ((settings.seconds_mode == 1)||((settings.seconds_mode == 2)&&temporary_second)) {
      float second_angle = TRIG_MAX_ANGLE * mode_time.seconds / 60;
      GPoint second_hand = (GPoint) {
        .x = (int16_t)(sin_lookup(second_angle) * (int32_t)(radius - HAND_MARGIN) / TRIG_MAX_RATIO) + center.x,
        .y = (int16_t)(-cos_lookup(second_angle) * (int32_t)(radius - HAND_MARGIN) / TRIG_MAX_RATIO) + center.y,
      };
      graphics_context_set_stroke_color(ctx, seconds_color);
      graphics_draw_line(ctx, center, second_hand);
    }
  }
}
