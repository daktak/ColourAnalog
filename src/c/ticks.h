/*
 * ticks.h
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */
#pragma once
#ifndef TICKS_H
#define TICKS_H

#include "settings.h"
void draw_ticks(GPoint s_center, ClaySettings settings, GContext *ctx, int16_t width, int16_t height);

#endif /* !TICKS_H */
