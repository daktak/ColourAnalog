/*
 * main.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * TODO
 * String
 * coin
 * Aesthetics
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "main.h"

#include "health.h"
#include "weather.h"

static Window *s_main_window;
static Layer *s_canvas_layer;
static Layer *s_date_layer;
static Layer *s_time_layer;

static GPoint s_center;
static Time s_last_time;
static uint8_t s_radius = 0;
static BluetoothLayer *s_bluetooth_layer;
static BatteryBarLayer *s_battery_layer;
static PhoneBattBarLayer *s_phone_batt_layer;
static Layer *s_weather_layer;
static Layer *s_health_layer;
time_t last_tap;
bool temporary_second;
static GPoint points[8];

/************************************ UI **************************************/

void redraw() {
  if (s_canvas_layer) {
    layer_mark_dirty(s_canvas_layer);
  }
}

void set_hidden(Layer *layer, bool hide) {
  if (layer != NULL) {
    layer_set_hidden(layer, hide);
  }
}

void text_layer_hide(bool hide) {
  set_hidden(s_weather_layer, hide);
  set_hidden(s_battery_layer, hide);
  set_hidden(s_health_layer, hide);
  set_hidden(s_bluetooth_layer, hide);
  set_hidden(s_date_layer, hide);
  set_hidden(s_time_layer, hide);
  set_hidden(s_phone_batt_layer, hide);
}

void reload_bt() {
  ClaySettings settings = get_settings();
  Layer *window_layer = window_get_root_layer(s_main_window);

  if (s_bluetooth_layer != NULL) {
    bluetooth_layer_destroy(s_bluetooth_layer);
  }
  if (get_position(BLUETOOTH) >= 0) {
    s_bluetooth_layer = create_bt_layer(settings, points);
    layer_add_child(window_layer, s_bluetooth_layer);
  }
}

void reload_batt() {
  ClaySettings settings = get_settings();
  Layer *window_layer = window_get_root_layer(s_main_window);

  if (s_battery_layer != NULL) {
    battery_bar_layer_destroy(s_battery_layer);
  }
  if (get_position(BATTERY) >= 0) {
    s_battery_layer = create_batt_bar(settings, points);
    layer_add_child(window_layer, s_battery_layer);
  }
}

void reload_pbb() {
  ClaySettings settings = get_settings();
  Layer *window_layer = window_get_root_layer(s_main_window);

  if (s_phone_batt_layer != NULL) {
    phone_batt_bar_layer_destroy(s_phone_batt_layer);
  }
  if (get_position(PHONE) >= 0) {
    s_phone_batt_layer = create_phone_batt_bar(settings, points);
    layer_add_child(window_layer, s_phone_batt_layer);
  }
}

void reload_health() {
  ClaySettings settings = get_settings();
  Layer *window_layer = window_get_root_layer(s_main_window);

  //health_destroy();
  //layer_safe_destroy(s_health_layer);
  if (get_position(HEALTH) >= 0) {
    set_hidden(s_health_layer, false);
    s_health_layer = create_health_layer(settings, points);
    layer_add_child(window_layer, s_health_layer);
  } else {
    set_hidden(s_health_layer, true);
  }
}

void reload_weather() {
  ClaySettings settings = get_settings();
  Layer *window_layer = window_get_root_layer(s_main_window);

  //destroy_weather();
  //layer_safe_destroy(s_weather_layer);
  if (get_position(WEATHER) >= 0) {
    set_hidden(s_weather_layer, false);
    s_weather_layer = create_weather_layer(settings, points);
    layer_add_child(window_layer, s_weather_layer);
    generic_weather_fetch(weather_callback);
  } else {
    set_hidden(s_weather_layer, true);
  }
}

void reload_date() {
  ClaySettings settings = get_settings();
  Layer *window_layer = window_get_root_layer(s_main_window);
  if (get_position(DATE) >= 0) {
    set_hidden(s_date_layer, false);
    s_date_layer = create_date_layer(settings, points);
    layer_add_child(window_layer, s_date_layer);
    set_date_text();
  } else {
    set_hidden(s_date_layer, true);
  }
}

void reload_time() {
  ClaySettings settings = get_settings();
  Layer *window_layer = window_get_root_layer(s_main_window);
  if (get_position(TIME) >= 0) {
    set_hidden(s_time_layer, false);
    s_time_layer = create_time_layer(settings, points);
    layer_add_child(window_layer, s_time_layer);
    set_time_text();
  } else {
    set_hidden(s_time_layer, true);
  }
}

void calc_bounds(GRect bounds) {
  uint16_t width = bounds.size.w;
  uint16_t height = bounds.size.h;
  uint8_t round_margin = PBL_IF_ROUND_ELSE(10, 0);
  GPoint new_points[] = {
    { width / 4 + round_margin, height / 4 + round_margin},
    { width / 2, height / 4},
    { width * 3/4 - round_margin, height / 4 + round_margin},
    { width/ 4, height / 2},
    //{ W_WIDTH / 2, W_HEIGHT / 2}, //middle shouldn't be used
    { width * 3/4, height / 2},
    { width / 4 + round_margin, height * 3/4 - round_margin},
    { width / 2, height * 3/4},
    { width * 3/4 - round_margin, height * 3/4 - round_margin}
  };
  memcpy(points, new_points, sizeof(new_points));
}

void create_text_layer() {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "create_text_layer");
#endif
  Layer *window_layer = window_get_root_layer(s_main_window);
  GRect bounds = layer_get_unobstructed_bounds(window_layer);
  calc_bounds(bounds);
  ClaySettings settings = get_settings();

  if (get_position(BLUETOOTH) >= 0) {
    s_bluetooth_layer = create_bt_layer(settings, points);
    layer_add_child(window_layer, s_bluetooth_layer);
  }

  if (get_position(BATTERY) >= 0) {
    s_battery_layer = create_batt_bar(settings, points);
    layer_add_child(window_layer, s_battery_layer);
  }

  if (get_position(PHONE) >= 0) {
    s_phone_batt_layer = create_phone_batt_bar(settings, points);
    layer_add_child(window_layer, s_phone_batt_layer);
  }

  if (get_position(HEALTH) >= 0) {
    s_health_layer = create_health_layer(settings, points);
    layer_add_child(window_layer, s_health_layer);
  }

  if (get_position(DATE) >= 0) {
    s_date_layer = create_date_layer(settings, points);
    layer_add_child(window_layer, s_date_layer);
    set_date_text();
  }

  if (get_position(TIME) >= 0) {
    s_time_layer = create_time_layer(settings, points);
    layer_add_child(window_layer, s_time_layer);
    set_time_text();
  }

  if (get_position(WEATHER) >= 0) {
    s_weather_layer = create_weather_layer(settings, points);
    layer_add_child(window_layer, s_weather_layer);
  }
}

void prv_tick_handler(struct tm *tick_time, TimeUnits changed) {
  // Store time
  s_last_time.hours = tick_time->tm_hour;
  s_last_time.hours -= (s_last_time.hours > 12) ? 12 : 0;
  s_last_time.minutes = tick_time->tm_min;
  s_last_time.seconds = tick_time->tm_sec;

  ClaySettings settings = get_settings();

  if ((get_position(TIME) >= 0) && (s_time_layer != NULL)) {
    set_time_text();
  }

  if(tick_time->tm_min % 30 == 0) {
    if ((get_position(WEATHER) >= 0) && (!quiet_time_is_active())) {
      generic_weather_fetch(weather_callback);
    }
  }
  //midnight
  if (tick_time->tm_min == 0 && tick_time->tm_hour ==0) {
    if ((get_position(DATE) >=0)&& (s_date_layer != NULL)) {
      set_date_text();
    }
  }

  if (temporary_second) {
    time_t now = time(NULL);
    if (now - last_tap > settings.seconds_duration*60) {
      temporary_second = false;
      tick_timer_service_unsubscribe();
      set_ticker();
    }
  }

  // Redraw
  redraw();
}

void prv_update_proc(Layer *layer, GContext *ctx) {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "prv_update_proc");
#endif
  ClaySettings settings = get_settings();
  GRect full_bounds = layer_get_bounds(layer);
  GRect bounds = layer_get_unobstructed_bounds(layer);
  GColor bg_color = GColorFromHEX(settings.bg_color);
  s_center = grect_center_point(&bounds);
  uint16_t width = bounds.size.w;
  uint16_t height = bounds.size.h;

  graphics_context_set_fill_color(ctx, bg_color);
  graphics_fill_rect(ctx, full_bounds, 0, GCornerNone);

  graphics_context_set_antialiased(ctx, ANTIALIASING);

  draw_hands(s_last_time, settings, ctx, s_radius, s_center, temporary_second);

  draw_ticks(s_center, settings, ctx, width, height);
}

void prv_unobstructed_will_change(GRect final_unobstructed_screen_area,
void *context) {
  // Get the full size of the screen
  GRect full_bounds = layer_get_bounds(s_canvas_layer);
  if (!grect_equal(&full_bounds, &final_unobstructed_screen_area)) {
    // Screen is about to become obstructed, hide the date
    text_layer_hide(true);
  }
}

void prv_unobstructed_did_change(void *context) {
  // Get the full size of the screen
  GRect full_bounds = layer_get_bounds(s_canvas_layer);
  // Get the total available screen real-estate
  GRect bounds = layer_get_unobstructed_bounds(s_canvas_layer);
  if (grect_equal(&full_bounds, &bounds)) {
    // Screen is no longer obstructed, show the date
    text_layer_hide(false);
  }
}

void prv_create_canvas(Window *s_window) {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "prv_create_canvas");
#endif
  Layer *window_layer = window_get_root_layer(s_window);
  GRect bounds = layer_get_unobstructed_bounds(window_layer);
  calc_bounds(bounds);

  s_radius = (bounds.size.w - 4) / 2;

  s_canvas_layer = layer_create(bounds);
  layer_set_update_proc(s_canvas_layer, prv_update_proc);
  layer_add_child(window_layer, s_canvas_layer);
  create_text_layer();
}

/*********************************** App **************************************/
void set_ticker() {
  ClaySettings settings = get_settings();
  if ((settings.seconds_mode == 1)||((settings.seconds_mode == 2)&&temporary_second)) {
    tick_timer_service_subscribe(SECOND_UNIT, prv_tick_handler);
  } else {
    tick_timer_service_subscribe(MINUTE_UNIT, prv_tick_handler);
  }
}

void tap_recieved(AccelAxisType axis, int32_t direction) {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "tap_recieved");
#endif
  ClaySettings settings = get_settings();
  if ((settings.seconds_mode == 2)&&(settings.seconds_duration > 0)&&(!quiet_time_is_active())) {
    time_t now = time(NULL);
    bool double_tap = (now - last_tap < 2);
    last_tap = now;
    if (double_tap) {
#ifdef DEBUG
      APP_LOG(APP_LOG_LEVEL_INFO, "double tap_recieved");
#endif
    } else if (!temporary_second) {
      temporary_second = true;
      tick_timer_service_unsubscribe();
      set_ticker();
    }
  }
}

void prv_window_load(Window *window) {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "prv_window_load");
#endif
  prv_create_canvas(window);
  set_ticker();
  // Subscribe to the will_change and did_change events
  UnobstructedAreaHandlers handlers = {
    .will_change = prv_unobstructed_will_change,
    .did_change = prv_unobstructed_did_change
  };
  unobstructed_area_service_subscribe(handlers, NULL);
  accel_tap_service_subscribe(tap_recieved);
}

void layer_safe_destroy(Layer *layer) {
  if (layer != NULL) {
    layer_destroy(layer);
  }
}

void prv_window_unload(Window *window) {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "prv_window_unload");
#endif
  accel_tap_service_unsubscribe();
  tick_timer_service_unsubscribe();
  layer_safe_destroy(s_canvas_layer);
  if (get_position(BLUETOOTH) >= 0) {
    bluetooth_layer_destroy(s_bluetooth_layer);
  }
  if (get_position(BATTERY) >= 0) {
    battery_bar_layer_destroy(s_battery_layer);
  }
  if (get_position(PHONE) >0) {
    phone_batt_bar_layer_destroy(s_phone_batt_layer);
    phone_batt_bar_save(PBB_KEY);
  }
  health_destroy();
  layer_safe_destroy(s_health_layer);
  destroy_weather();
  layer_safe_destroy(s_weather_layer);
  layer_safe_destroy(s_date_layer);
  layer_safe_destroy(s_time_layer);
}

static void inbox_dropped_callback(AppMessageResult reason, void *context)
{
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_ERROR, "Message dropped!");
#endif
}

static void outbox_failed_callback(DictionaryIterator *iterator, AppMessageResult reason, void *context)
{
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_ERROR, "Outbox send failed!");
#endif
}

static void outbox_sent_callback(DictionaryIterator *iterator, void *context)
{
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "Outbox send success!");
#endif
}

void prv_init() {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "prv_init");
#endif
  prv_load_settings();
  srand(time(NULL));

  time_t t = time(NULL);
  struct tm *time_now = localtime(&t);
  prv_tick_handler(time_now, MINUTE_UNIT);

  s_main_window = window_create();
  window_set_window_handlers(s_main_window, (WindowHandlers) {
    .load = prv_window_load,
    .unload = prv_window_unload,
  });
  window_stack_push(s_main_window, true);
  int inbox_size = app_message_inbox_size_maximum();
#ifdef PBL_PLATFORM_APLITE
    inbox_size = 800;
#endif
  events_app_message_request_inbox_size(inbox_size);
  //events_app_message_request_outbox_size(app_message_outbox_size_maximum());
  events_app_message_register_inbox_received(prv_inbox_received_handler, NULL);
  events_app_message_register_inbox_dropped(inbox_dropped_callback, NULL);
  events_app_message_register_outbox_failed(outbox_failed_callback, NULL);
  events_app_message_register_outbox_sent(outbox_sent_callback, NULL);
  events_app_message_open();
}

void prv_deinit() {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "prv_deint");
#endif
  window_destroy(s_main_window);
}

int main() {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "main");
#endif
  prv_init();
  app_event_loop();
  prv_deinit();
}
