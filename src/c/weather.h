/*
 * weather.h
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */
#pragma once

#ifndef WEATHER_H
#define WEATHER_H

#include "main.h"
Layer* create_weather_layer(ClaySettings settings, GPoint points[]);
void weather_callback(GenericWeatherInfo *info, GenericWeatherStatus status);
void destroy_weather();

#endif /* !WEATHER_H */
