/*
 * health.h
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */
#pragma once

#ifndef STEPS_H
#define STEPS_H

#include "main.h"
Layer* create_health_layer(ClaySettings settings, GPoint points[]);
void health_destroy();

#endif /* !STEPS_H */
