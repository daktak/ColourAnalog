/*
 * settings.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */
#include "main.h"
#include "weather.h"

static ClaySettings settings;
/*
void log_int(int num) {
  static char s_buffer[10];
  snprintf(s_buffer, 10, "%i", num);
  APP_LOG(APP_LOG_LEVEL_INFO, s_buffer);
}*/

ClaySettings get_settings() {
  return settings;
}

int get_position(int val) {
  for (int i=0; i<8; i++) {
    if (settings.location_array[i] == val) {
      return i;
    }
  }
  return -1;
}

static void prv_save_settings() {
  persist_write_data(SETTINGS_KEY, &settings, sizeof(settings));
}

static void prv_default_settings() {
  settings.bg_color = 0x000000;
  settings.hh_color = PBL_IF_COLOR_ELSE(0xFFAAAA, 0xFFFFFF);
  settings.ht_color = 0xFFFFFF;
  settings.mh_color = 0xFFFFFF;
  settings.mt_color = PBL_IF_COLOR_ELSE(0x55AAFF, 0xAAAAAA);
  settings.sh_color = PBL_IF_COLOR_ELSE(0xAA0000, 0xFFFFFF);
  settings.bt_vibe_discon = true;
  settings.bt_vibe_connect = false;
  settings.bt_fg_color = 0x000000;
  settings.bt_d_fg_color = 0xFFFFFF;
  settings.batt_hide_icon = false;
  settings.batt_hide_percent = true;
  settings.batt_normal_color = 0xFFFFFF;
  settings.batt_warning_color = PBL_IF_COLOR_ELSE(0xFFFF00, 0xFFFFFF);
  settings.batt_danger_color = PBL_IF_COLOR_ELSE(0xFF5500, 0xFFFFFF);
  settings.batt_charge_color = PBL_IF_COLOR_ELSE(0x00AA00, 0xFFFFFF);
  settings.health_fg_color = 0xFFFFFF;
  settings.health_metric = 1;
  settings.weather_fg_color = 0xFFFFFF;
  settings.weather_celcius = true;
  settings.weather_feels_like = true;
  strcpy(settings.weather_api, "31196cb8a000e808be9f27de97a6f2e1");
  settings.weather_provider = GenericWeatherProviderOpenWeatherMap;
  settings.date_color = 0xFFFFFF;
  settings.date_format = 5;
  settings.time_color = 0xFFFFFF;
  memcpy(settings.location_array, (int[8]){0,BATTERY,BLUETOOTH,WEATHER,DATE,0,TIME,0}, sizeof(settings.location_array));
  settings.seconds_duration = 2;
  settings.seconds_mode = 0;
  settings.pbb_hide_icon = false;
  settings.pbb_hide_percent = true;
  settings.pbb_normal_color = 0xFFFFFF;
  settings.pbb_warning_color = PBL_IF_COLOR_ELSE(0xFFFF00, 0xFFFFFF);
  settings.pbb_danger_color = PBL_IF_COLOR_ELSE(0xFF5500, 0xFFFFFF);
  settings.pbb_charge_color = PBL_IF_COLOR_ELSE(0x00AA00, 0xFFFFFF);
}

void prv_load_settings() {
  prv_default_settings();
  persist_read_data(SETTINGS_KEY, &settings, sizeof(settings));
}

void location_message(DictionaryIterator *iter, void *context) {
  Tuple *north_west_t = dict_find(iter, MESSAGE_KEY_NorthWest);
  Tuple *north_t = dict_find(iter, MESSAGE_KEY_North);
  Tuple *north_east_t = dict_find(iter, MESSAGE_KEY_NorthEast);
  Tuple *west_t = dict_find(iter, MESSAGE_KEY_West);
  Tuple *east_t = dict_find(iter, MESSAGE_KEY_East);
  Tuple *south_west_t = dict_find(iter, MESSAGE_KEY_SouthWest);
  Tuple *south_t = dict_find(iter, MESSAGE_KEY_South);
  Tuple *south_east_t = dict_find(iter, MESSAGE_KEY_SouthEast);

  if (north_west_t) {
    settings.location_array[0] = atoi(north_west_t->value->cstring);
  }
  if (north_t) {
    settings.location_array[1] = atoi(north_t->value->cstring);
  }
  if (north_east_t) {
    settings.location_array[2] = atoi(north_east_t->value->cstring);
  }
  if (west_t) {
    settings.location_array[3] = atoi(west_t->value->cstring);
  }
  if (east_t) {
    settings.location_array[4] = atoi(east_t->value->cstring);
  }
  if (south_west_t) {
    settings.location_array[5] = atoi(south_west_t->value->cstring);
  }
  if (south_t) {
    settings.location_array[6] = atoi(south_t->value->cstring);
  }
  if (south_east_t) {
    settings.location_array[7] = atoi(south_east_t->value->cstring);
  }
}

void weather_message(DictionaryIterator *iter, void *context) {
  bool refresh = false;
  Tuple *weather_feels_like_t = dict_find(iter, MESSAGE_KEY_WeatherFeelsLike);
  Tuple *weather_celcius_t = dict_find(iter, MESSAGE_KEY_WeatherCelcius);
  Tuple *weather_api_t = dict_find(iter, MESSAGE_KEY_WeatherApi);
  Tuple *weather_provider_t = dict_find(iter, MESSAGE_KEY_WeatherProvider);
  Tuple *weather_color_t = dict_find(iter, MESSAGE_KEY_WeatherColor);

  if (weather_feels_like_t) {
    bool old = settings.weather_feels_like;
    settings.weather_feels_like = (bool) (weather_feels_like_t->value->uint32 == 1);
    refresh = (old != settings.weather_feels_like) || refresh;
  }

  if (weather_celcius_t) {
    bool old = settings.weather_celcius;
    settings.weather_celcius = (bool) (weather_celcius_t->value->uint32 == 1);
    refresh = (old != settings.weather_celcius) || refresh;
  }

  if (weather_api_t) {
    char old[50];
    strcpy(old, settings.weather_api);
    strcpy(settings.weather_api, weather_api_t->value->cstring);
    refresh = (strcmp(old,settings.weather_api) != 0) || refresh;
  }

  if (weather_provider_t) {
    int old = settings.weather_provider;
    settings.weather_provider = atoi(weather_provider_t->value->cstring);
    refresh = (old != settings.weather_provider) || refresh;
  }

  if (weather_color_t) {
    int old = settings.weather_fg_color;
    settings.weather_fg_color = weather_color_t->value->uint32;
    refresh = (old != settings.weather_fg_color) || refresh;
  }

  if (refresh) {
    reload_weather();
  }
}

void date_message(DictionaryIterator *iter, void *context) {
  bool refresh = false;
  Tuple *date_color_t = dict_find(iter, MESSAGE_KEY_DateColor);
  Tuple *date_format_t = dict_find(iter, MESSAGE_KEY_DateFormat);

  if (date_color_t) {
    int old = settings.date_color;
    settings.date_color= date_color_t->value->uint32;
    refresh = (old != settings.date_color) || refresh;
  }

  if (date_format_t) {
    int old = settings.date_format;
    settings.date_format = atoi(date_format_t->value->cstring);
    refresh = (old != settings.date_format) || refresh;
  }

  if (refresh) {
    reload_date();
  }
}

void time_message(DictionaryIterator *iter, void *context) {
  bool refresh = false;
  Tuple *time_color_t = dict_find(iter, MESSAGE_KEY_TimeColor);

  if (time_color_t) {
    int old = settings.time_color;
    settings.time_color= time_color_t->value->uint32;
    refresh = (old != settings.time_color) || refresh;
  }

  if (refresh) {
    reload_time();
  }
}

void health_message(DictionaryIterator *iter, void *context) {
  bool refresh = false;
  Tuple *health_fg_color_t = dict_find(iter, MESSAGE_KEY_HealthColor);
  Tuple *health_metric_t = dict_find(iter, MESSAGE_KEY_HealthMetric);

  if (health_fg_color_t) {
    int old = settings.health_fg_color;
    settings.health_fg_color = health_fg_color_t->value->uint32;
    refresh = (old != settings.health_fg_color) || refresh;
  }

  if (health_metric_t) {
    int old = settings.health_metric;
    settings.health_metric = atoi(health_metric_t->value->cstring);
    refresh = (old != settings.health_metric) || refresh;
  }

  if (refresh) {
    reload_health();
  }
}

void bt_message(DictionaryIterator *iter, void *context) {
  //Bluetooth
  bool refresh = false;
  Tuple *bt_vibe_discon_t = dict_find(iter, MESSAGE_KEY_BtVibeDiscon);
  Tuple *bt_vibe_connect_t = dict_find(iter, MESSAGE_KEY_BtVibeConnect);
  Tuple *bt_fg_color_t = dict_find(iter, MESSAGE_KEY_BtFGColor);
  Tuple *bt_d_fg_color_t = dict_find(iter, MESSAGE_KEY_BtDFGColor);

  if (bt_vibe_discon_t) {
    bool old = settings.bt_vibe_discon;
    settings.bt_vibe_discon = (bool) (bt_vibe_discon_t->value->uint32 == 1);
    refresh = (old != settings.bt_vibe_discon) || refresh;
  }

  if (bt_vibe_connect_t) {
    bool old = settings.bt_vibe_connect;
    settings.bt_vibe_connect = (bool) (bt_vibe_connect_t->value->uint32 == 1);
    refresh = (old != settings.bt_vibe_connect) || refresh;
  }

  if (bt_fg_color_t) {
    int old = settings.bt_fg_color;
    settings.bt_fg_color = bt_fg_color_t->value->uint32;
    refresh = (old != settings.bt_fg_color) || refresh;
  }

  if (bt_d_fg_color_t) {
    int old = settings.bt_d_fg_color;
    settings.bt_d_fg_color = bt_d_fg_color_t->value->uint32;
    refresh = (old != settings.bt_d_fg_color) || refresh;
  }

  if (refresh) {
    reload_bt();
  }
}

void pbb_message(DictionaryIterator *iter, void *context) {
  //Phone Battery Bar
  bool refresh = false;
  Tuple *pbb_hide_icon_t = dict_find(iter, MESSAGE_KEY_PbbHideIcon);
  Tuple *pbb_hide_percent_t = dict_find(iter, MESSAGE_KEY_PbbHidePercent);
  Tuple *pbb_normal_color_t = dict_find(iter, MESSAGE_KEY_PbbNormalColor);
  Tuple *pbb_warning_color_t = dict_find(iter, MESSAGE_KEY_PbbWarningColor);
  Tuple *pbb_danger_color_t = dict_find(iter, MESSAGE_KEY_PbbDangerColor);
  Tuple *pbb_charge_color_t = dict_find(iter, MESSAGE_KEY_PbbChargeColor);

  if (pbb_hide_icon_t) {
    bool old = settings.pbb_hide_icon;
    settings.pbb_hide_icon = (bool) (pbb_hide_icon_t->value->uint32 == 1);
    refresh = (old != settings.pbb_hide_icon) || refresh;
  }

  if (pbb_hide_percent_t) {
    bool old = settings.pbb_hide_percent;
    settings.pbb_hide_percent = (bool) (pbb_hide_percent_t->value->uint32 == 1);
    refresh = (old != settings.pbb_hide_percent) || refresh;
  }

  if (pbb_normal_color_t) {
    int old = settings.pbb_normal_color;
    settings.pbb_normal_color = pbb_normal_color_t->value->uint32;
    refresh = (old != settings.pbb_normal_color) || refresh;
  }

  if (pbb_warning_color_t) {
    int old = settings.pbb_warning_color;
    settings.pbb_warning_color = pbb_warning_color_t->value->uint32;
    refresh = (old != settings.pbb_warning_color) || refresh;
  }

  if (pbb_danger_color_t) {
    int old = settings.pbb_danger_color;
    settings.pbb_danger_color = pbb_danger_color_t->value->uint32;
    refresh = (old != settings.pbb_danger_color) || refresh;
  }

  if (pbb_charge_color_t) {
    int old = settings.pbb_charge_color;
    settings.pbb_charge_color = pbb_charge_color_t->value->uint32;
    refresh = (old != settings.pbb_charge_color) || refresh;
  }

  if (refresh) {
    reload_pbb();
  }
}

void batt_message(DictionaryIterator *iter, void *context) {
  //Battery Bar
  bool refresh = false;
  Tuple *batt_hide_icon_t = dict_find(iter, MESSAGE_KEY_BattHideIcon);
  Tuple *batt_hide_percent_t = dict_find(iter, MESSAGE_KEY_BattHidePercent);
  Tuple *batt_normal_color_t = dict_find(iter, MESSAGE_KEY_BattNormalColor);
  Tuple *batt_warning_color_t = dict_find(iter, MESSAGE_KEY_BattWarningColor);
  Tuple *batt_danger_color_t = dict_find(iter, MESSAGE_KEY_BattDangerColor);
  Tuple *batt_charge_color_t = dict_find(iter, MESSAGE_KEY_BattChargeColor);

  if (batt_hide_icon_t) {
    bool old = settings.batt_hide_icon;
    settings.batt_hide_icon = (bool) (batt_hide_icon_t->value->uint32 == 1);
    refresh = (old != settings.batt_hide_icon) || refresh;
  }

  if (batt_hide_percent_t) {
    bool old = settings.batt_hide_percent;
    settings.batt_hide_percent = (bool) (batt_hide_percent_t->value->uint32 == 1);
    refresh = (old != settings.batt_hide_percent) || refresh;
  }

  if (batt_normal_color_t) {
    int old = settings.batt_normal_color;
    settings.batt_normal_color = batt_normal_color_t->value->uint32;
    refresh = (old != settings.batt_normal_color) || refresh;
  }

  if (batt_warning_color_t) {
    int old = settings.batt_warning_color;
    settings.batt_warning_color = batt_warning_color_t->value->uint32;
    refresh = (old != settings.batt_warning_color) || refresh;
  }

  if (batt_danger_color_t) {
    int old = settings.batt_danger_color;
    settings.batt_danger_color = batt_danger_color_t->value->uint32;
    refresh = (old != settings.batt_danger_color) || refresh;
  }

  if (batt_charge_color_t) {
    int old = settings.batt_charge_color;
    settings.batt_charge_color = batt_charge_color_t->value->uint32;
    refresh = (old != settings.batt_charge_color) || refresh;
  }

  if (refresh) {
    reload_batt();
  }
}

void hand_message(DictionaryIterator *iter, void *context) {
  bool refresh = false;
  //hands
  Tuple *bg_color_t = dict_find(iter, MESSAGE_KEY_BackgroundColor);
  Tuple *hh_color_t = dict_find(iter, MESSAGE_KEY_HourHandColor);
  Tuple *ht_color_t = dict_find(iter, MESSAGE_KEY_HourTicksColor);
  Tuple *mh_color_t = dict_find(iter, MESSAGE_KEY_MinuteHandColor);
  Tuple *mt_color_t = dict_find(iter, MESSAGE_KEY_MinuteTicksColor);
  Tuple *sh_color_t = dict_find(iter, MESSAGE_KEY_SecHandColor);
  Tuple *sec_mode_t = dict_find(iter, MESSAGE_KEY_SecMode);
  Tuple *sec_duration_t = dict_find(iter, MESSAGE_KEY_SecDuration);

  if (bg_color_t) {
    int old = settings.bg_color;
    settings.bg_color = bg_color_t->value->uint32;
    refresh = (old != settings.bg_color) || refresh;
  }

  if (hh_color_t) {
    int old = settings.hh_color;
    settings.hh_color = hh_color_t->value->uint32;
    refresh = (old != settings.hh_color) || refresh;
  }

  if (ht_color_t) {
    int old = settings.ht_color;
    settings.ht_color = ht_color_t->value->uint32;
    refresh = (old != settings.ht_color) || refresh;
  }

  if (mh_color_t) {
    int old = settings.mh_color;
    settings.mh_color = mh_color_t->value->uint32;
    refresh = (old != settings.mh_color) || refresh;
  }

  if (mt_color_t) {
    int old = settings.mt_color;
    settings.mt_color = mt_color_t->value->uint32;
    refresh = (old != settings.mt_color) || refresh;
  }

  if (sh_color_t) {
    int old = settings.sh_color;
    settings.sh_color = sh_color_t->value->uint32;
    refresh = (old != settings.sh_color) || refresh;
  }

  if (sec_mode_t) {
    int old = settings.seconds_mode;
    settings.seconds_mode = atoi(sec_mode_t->value->cstring);
    if (old != settings.seconds_mode) {
      tick_timer_service_unsubscribe();
      set_ticker();
    }
  }

  if (sec_duration_t) {
    settings.seconds_duration = sec_duration_t->value->uint32;
  }

  if (refresh) {
    redraw();
  }
}


void prv_inbox_received_handler(DictionaryIterator *iter, void *context) {
  Tuple *data = dict_find(iter, MESSAGE_KEY_READY);
  if (data) {
    generic_weather_fetch(weather_callback);
  }
  location_message(iter, context);
  hand_message(iter, context);
  bt_message(iter, context);
  batt_message(iter, context);
  pbb_message(iter, context);
  health_message(iter, context);
  weather_message(iter, context);
  date_message(iter, context);
  time_message(iter, context);
  prv_save_settings();
}
