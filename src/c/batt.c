/*
 * batt.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "main.h"
#include "point.h"

static int height = 10;
static int width = 20;
static int offset = 0;

BatteryBarLayer* create_batt_bar(ClaySettings settings, GPoint points[]) {
  BatteryBarLayer *s_battery_layer = battery_bar_layer_create();
  int pos = get_position(BATTERY);
  if (pos >= 0) {
    if (!settings.batt_hide_icon&&!settings.batt_hide_percent) {
      width = 40;
    } else if (settings.batt_hide_percent) {
      offset = 20;
    }
    GPoint batt_point = center_point(points, width, height, pos);
    batt_point.x = batt_point.x - offset;
    battery_bar_set_position(batt_point);
    battery_bar_set_percent_hidden(settings.batt_hide_percent);
    battery_bar_set_icon_hidden(settings.batt_hide_icon);
    battery_bar_set_colors(GColorFromHEX(settings.batt_normal_color), GColorFromHEX(settings.batt_warning_color),
        GColorFromHEX(settings.batt_danger_color), GColorFromHEX(settings.batt_charge_color));
  } else {
    battery_bar_set_icon_hidden(true);
    battery_bar_set_percent_hidden(true);
  }
  return s_battery_layer;
}


