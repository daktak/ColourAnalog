/*
 * batt.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "main.h"
#include "point.h"

static int height = 10;
static int width = 20;
static int offset = 0;

PhoneBattBarLayer* create_phone_batt_bar(ClaySettings settings, GPoint points[]) {
  PhoneBattBarLayer *s_phone_batt_layer = phone_batt_bar_layer_create();
  int pos = get_position(PHONE);
  if (pos >= 0) {
    if (!settings.pbb_hide_icon&&!settings.pbb_hide_percent) {
      width = 40;
    } else if (settings.pbb_hide_percent) {
      offset = 20;
    }
    GPoint batt_point = center_point(points, width, height, pos);
    batt_point.x = batt_point.x - offset;
    phone_batt_bar_load(PBB_KEY);
    phone_batt_bar_set_position(batt_point);
    phone_batt_bar_set_percent_hidden(settings.pbb_hide_percent);
    phone_batt_bar_set_icon_hidden(settings.pbb_hide_icon);
    phone_batt_bar_set_colors(GColorFromHEX(settings.pbb_normal_color), GColorFromHEX(settings.pbb_warning_color),
        GColorFromHEX(settings.pbb_danger_color), GColorFromHEX(settings.pbb_charge_color));
  } else {
    phone_batt_bar_set_icon_hidden(true);
    phone_batt_bar_set_percent_hidden(true);
  }
  return s_phone_batt_layer;
}


