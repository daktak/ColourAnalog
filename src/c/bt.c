/*
 * bt.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "main.h"
#include "point.h"

static int width = 14;
static int height = 14;

BluetoothLayer* create_bt_layer(ClaySettings settings, GPoint points[]) {
  BluetoothLayer *s_bluetooth_layer = bluetooth_layer_create();
  int pos = get_position(BLUETOOTH);
  if (pos >= 0) {
    GPoint bt_point = center_point(points, width, height, pos);
    bluetooth_set_position(bt_point);
    bluetooth_vibe_disconnect(settings.bt_vibe_discon);
    bluetooth_vibe_connect(settings.bt_vibe_connect);
    bluetooth_set_colors(PBL_IF_COLOR_ELSE(GColorClear, GColorFromHEX(settings.bg_color)), GColorFromHEX(settings.bt_fg_color),
        PBL_IF_COLOR_ELSE(GColorClear, GColorFromHEX(settings.bg_color)), GColorFromHEX(settings.bt_d_fg_color));
  }
  return s_bluetooth_layer;

}
