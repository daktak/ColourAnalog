/*
 * settings.h
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#pragma once

#ifndef SETTINGS_H
#define SETTINGS_H

#define SETTINGS_KEY 1

#include <pebble.h>
typedef struct ClaySettings {
  int bg_color;
  int ht_color;
  int mt_color;
  int hh_color;
  int mh_color;
  int sh_color;
  bool sh_on;
  bool bt_vibe_discon;
  bool bt_vibe_connect;
  int bt_fg_color;
  int bt_d_fg_color;
  bool batt_hide_percent;
  bool batt_hide_icon;
  int batt_normal_color;
  int batt_warning_color;
  int batt_danger_color;
  int batt_charge_color;
  int health_fg_color;
  int health_metric;
  int weather_fg_color;
  bool weather_celcius;
  int weather_provider;
  char weather_api[50];
  bool weather_feels_like;
  int date_color;
  int date_format;
  int time_color;
  int location_array[8];
  int seconds_duration;
  int seconds_mode;
  bool pbb_hide_percent;
  bool pbb_hide_icon;
  int pbb_normal_color;
  int pbb_warning_color;
  int pbb_danger_color;
  int pbb_charge_color;
} ClaySettings;

void prv_inbox_received_handler(DictionaryIterator *iter, void *context);
void prv_load_settings();
int get_position(int val);
ClaySettings get_settings();

#endif /* !SETTINGS_H */
