/*
 * health.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "main.h"
#include "health.h"
#include "point.h"
static int width = 14*6;
static int height = 36;
static TextLayer *s_health_layer;
static int health_metric = 1;
static bool health_was_on = false;

void health_updated() {
  static char s_buffer[8];
  if ((s_health_layer != NULL)&&(get_position(HEALTH) >= 0)) {
    int metric = 0;
    switch (health_metric) {
      case 1: // steps
        metric = health_get_metric_sum(HealthMetricStepCount);
        break;
      case 2:
        metric = health_get_metric_sum(HealthMetricActiveSeconds);
        break;
      case 3:
        metric = health_get_metric_sum(HealthMetricWalkedDistanceMeters);
        break;
      case 4:
        metric = health_get_metric_sum(HealthMetricSleepSeconds);
        break;
      case 5:
        metric = health_get_metric_sum(HealthMetricSleepRestfulSeconds);
        break;
      case 6:
        metric = health_get_metric_sum(HealthMetricRestingKCalories);
        break;
      case 7:
        metric = health_get_metric_sum(HealthMetricActiveKCalories);
        break;
      case 8:
        metric = health_get_metric_sum(HealthMetricHeartRateBPM);
        break;
      default:
        metric = health_get_metric_sum(HealthMetricStepCount);
        break;
    }
    snprintf(s_buffer, sizeof(s_buffer), "%d", metric);
    text_layer_set_text(s_health_layer, s_buffer);
  }
}

Layer* create_health_layer(ClaySettings settings, GPoint points[]) {
  health_metric = settings.health_metric;
  int pos = get_position(HEALTH);
  GPoint start = center_point(points, width, height, pos);
  if (s_health_layer == NULL) {
    s_health_layer = text_layer_create (GRect(start.x, start.y, width, height));
  }
  text_layer_set_background_color(s_health_layer, GColorClear);
  text_layer_set_text_color(s_health_layer, GColorFromHEX(settings.health_fg_color));
  text_layer_set_font(s_health_layer, fonts_get_system_font(FONT_KEY_GOTHIC_14));
  text_layer_set_text_alignment(s_health_layer, GTextAlignmentCenter);

  if (pos >= 0) {
    health_init(health_updated);
    health_was_on = true;
  } else {
    if (health_was_on) {
      health_deinit();
      health_was_on = false;
    }
    if (s_health_layer != NULL) {
      text_layer_set_text(s_health_layer, "");
    }
  };

  return text_layer_get_layer(s_health_layer);
}

void health_destroy() {
#ifdef DEBUG
  APP_LOG(APP_LOG_LEVEL_INFO, "health_destroy");
#endif
  if (health_was_on) {
    health_deinit();
    health_was_on = false;
  }
}

