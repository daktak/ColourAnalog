/*
 * weather.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "weather.h"
#include "point.h"

TextLayer *s_weather_layer;
bool celcius = false;
bool weather_was_on = false;

void weather_callback(GenericWeatherInfo *info, GenericWeatherStatus status) {
  if (get_position(WEATHER) >= 0) {
    switch(status) {
      case GenericWeatherStatusAvailable:
      {
        static char s_buffer[256];
        int temp = info->temp_f;
        if (celcius) {
          temp = info->temp_c;
        }
        //put setting for temp c or f / desc on off
        snprintf(s_buffer, sizeof(s_buffer), "%d°", temp);
        text_layer_set_text(s_weather_layer, s_buffer);
      }
        break;
      case GenericWeatherStatusNotYetFetched:
#ifdef DEBUG
        APP_LOG(APP_LOG_LEVEL_INFO, "NotYetFetched");
#endif
        break;
      case GenericWeatherStatusBluetoothDisconnected:
#ifdef DEBUG
        APP_LOG(APP_LOG_LEVEL_INFO, "Bluetooth");
#endif
        break;
      case GenericWeatherStatusPending:
#ifdef DEBUG
        APP_LOG(APP_LOG_LEVEL_INFO, "Pending");
#endif
        break;
      case GenericWeatherStatusFailed:
#ifdef DEBUG
        APP_LOG(APP_LOG_LEVEL_INFO, "Failed");
#endif
        break;
      case GenericWeatherStatusBadKey:
#ifdef DEBUG
        APP_LOG(APP_LOG_LEVEL_INFO, "BadKey");
#endif
        break;
      case GenericWeatherStatusLocationUnavailable:
#ifdef DEBUG
        APP_LOG(APP_LOG_LEVEL_INFO, "Loc missing");
#endif
        break;
    }
  }
}

void destroy_weather() {
  APP_LOG(APP_LOG_LEVEL_INFO, "destroy_weather");
  if (weather_was_on) {
    generic_weather_deinit();
    weather_was_on = false;
  }
}

Layer* create_weather_layer(ClaySettings settings, GPoint points[]) {
  celcius = settings.weather_celcius;
  int height = 36;
  int width = 9*3;
  int pos = get_position(WEATHER);
  GPoint start = center_point(points, width, height, pos);
  if (s_weather_layer == NULL) {
    s_weather_layer = text_layer_create (GRect(start.x, start.y, width, height));
  }
  text_layer_set_background_color(s_weather_layer, GColorClear);
  text_layer_set_text_color(s_weather_layer, GColorFromHEX(settings.weather_fg_color));
  text_layer_set_font(s_weather_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
  text_layer_set_overflow_mode(s_weather_layer, GTextOverflowModeWordWrap);
  text_layer_set_text_alignment(s_weather_layer, GTextAlignmentCenter);

  if (pos >= 0) {
    generic_weather_init();
    generic_weather_set_api_key(settings.weather_api);
    generic_weather_set_provider(settings.weather_provider);
    generic_weather_set_feels_like(settings.weather_feels_like);
    weather_was_on = true;
  } else {
    if (weather_was_on) {
      generic_weather_deinit();
      weather_was_on = false;
    }
    text_layer_set_text(s_weather_layer, "");
  }

  return text_layer_get_layer(s_weather_layer);
}
