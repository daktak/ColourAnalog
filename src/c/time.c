/*
 * time.c
 * Copyright (C) 2018 daktak <daktak@domo.ddns.net>
 *
 * Distributed under terms of the GPLV3 license.
 */

#include "point.h"
static int width = 21*6;
static int height = 42;
TextLayer *s_time_text;

void set_time_text() {
  time_t now = time(NULL);
  struct tm *t = localtime(&now);
  static char s_buffer[sizeof("24:00")];
  strftime(s_buffer, sizeof(s_buffer), clock_is_24h_style() ?
                                                "%H:%M" : "%I:%M", t);
  if (s_time_text != NULL) {
    text_layer_set_text(s_time_text, s_buffer);
  }
}

Layer* create_time_layer(ClaySettings settings, GPoint points[]) {
  int pos = get_position(TIME);
  GPoint start = center_point(points, width, height, pos);
  if (s_time_text == NULL) {
    s_time_text = text_layer_create (GRect(start.x, start.y, width, height));
  }
  text_layer_set_background_color(s_time_text, GColorClear);
  text_layer_set_text_color(s_time_text, GColorFromHEX(settings.time_color));
  text_layer_set_font(s_time_text, fonts_get_system_font(FONT_KEY_ROBOTO_CONDENSED_21));
  text_layer_set_overflow_mode(s_time_text, GTextOverflowModeWordWrap);
  text_layer_set_text_alignment(s_time_text, GTextAlignmentCenter);
  //set_time_text();
  return text_layer_get_layer(s_time_text);
}
